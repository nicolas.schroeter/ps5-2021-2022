---
version: 2
titre: Miniaturisation de Eye+ pour l’intégrer dans l’AsyCube
type de projet: Projet de semestre 5
année scolaire: 2021/2022
abréviation: Mini Eye+
filières:
  - Télécommunications
  - Informatique
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Daniel Gachet
mots-clé: [systèmes embarqués, caméra, traitements d'images, performance]
langue: [F]
confidentialité: non
suite: non
---

\begin{center}
\includegraphics[width=0.5\textwidth]{img/asycube.png}
\end{center}

## Contexte/Objectifs

Asyril est une société basée à Villaz-St-Pierre (https://www.asyril.com/fr/). Elle est reconnue dans le monde entier pour ses systèmes innovants et performants pour l'automation et la robotique. Sa gamme de produits inclut des feeders, un système de localisation basé sur une caméra ainsi que des solutions robotisées. 

Asyril produit et développe une machine industrielle permettant d’arranger des pièces en vrac. L’AsyCube est l’élément qui permet de changer la disposition des pièces à l’aide de vibrations. Puis le système Eye+ prend des photographies, les analyse par un réseau de neurones et détermine le positionnement des différentes pièces ainsi que leur accessibilité. Les pièces accessibles sont ensuite enlevées par un robot piloté, puis disposées de manière précise sur un support. 

Asyril a pour objectif de diminuer la taille de Eye+ pour l’intégrer dans l’AsyCube tout en conversant les fonctionnalités et performances actuelles.

Les étapes de ce projet consistent à rechercher et choisir un système embarqué dédié de petites dimensions. Certaines fonctionnalités de traitement d’images et de reconnaissance des pièces seront intégrées dans ce nouveau matériel afin de tester et de valider les performances de la nouvelle solution.


## Tâches
Travaux à réaliser:

- Analyser la solution Eye+ actuelle
- Rechercher des solutions matérielles (systèmes embarqués) de petite dimension capable de connecter la caméra et d’exécuter les traitements d’images de reconnaissance des pièces
- Développer un logiciel qui permet d’acquérir les images de la caméra et d’effectuer des traitements intensifs d’images pour valider les performances du matériel dédié
- Elaborer une documentation technique complète et rédaction d’un rapport décrivant la démarche et les choix opérés.
