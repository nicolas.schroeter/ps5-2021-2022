---
version: 2
titre: Performances de l’Ultra WideBand
type de projet: Projet de semestre 5
année scolaire: 2021/2022
abréviation: UWB
filières:
  - Télécommunications
  - Informatique
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Serge Ayer
mots-clé: [UWB, performance, systèmes embarqués, localisation, sécurité]
langue: [F]
confidentialité: non
suite: non
---

\begin{center}
\includegraphics[width=0.5\textwidth]{img/uwb.png}
\end{center}

## Contexte/Objectifs

L’Ultra WideBand (UWB), qui est embarquée dans certains téléphones portables récents, est une technologie émergente de transmission radio à large bande et à courte portée. Elle offre des transmissions de données à haut débit, le positionnement indoor précis tout en utilisant peu d’énergie. Cette technologie est directement en concurrence avec les solutions dans le domaine des objets connectés telles que bluetooth, NFC, etc.

L’école dispose de cartes d’évaluation DW3000 de Decawave/Qorvo qui intègre cette technologie UWB. De plus, nous disposons des logiciels qui permettent de mettre en œuvre la communication, l’estimation de la portée ainsi que la localisation. Nous disposons également d’iPhone équipés de dispositifs UWB.

L’objectif du projet est d’évaluer les performances de la communication UWB et du positionnement indoor, ainsi que d’évaluer les possibilités offertes aujourd’hui sur les iPhones.


## Tâches
Travaux à réaliser:

- Analyser la technologie UWB
- Prendre en main l’environnement des cartes d’évaluation et faire fonctionner des exemples de communication et de localisation
- Prendre en main les applications disponibles sur iPhone, les tester avec différents dispositifs UWB et évaluer les possibilités offertes dans cet environnement
- Tester la bande passante, la portée et la localisation dans différentes situations
- Traiter les aspects de sécurité
- Elaborer une documentation technique complète et rédaction d’un rapport décrivant la démarche et les choix opérés.
